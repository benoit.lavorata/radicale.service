#!/bin/bash
echo ""
echo "Radical webDav/Cal"
echo "Maintainer: Benoit Lavorata"
echo "License: MIT"
echo "==="
echo ""

FILE=".env"
if [ -f $FILE ]; then
    echo ""
    echo "File $FILE exists, will now start: ..."
    export $(cat .env | xargs)

    echo ""
    echo "Create networks ..."
    docker network create $RADICALE_CONTAINER_NETWORK

    #echo ""
    #echo "Create volumes ..."
    docker volume create --name $RADICALE_CONTAINER_VOLUME_CONFIG
    docker volume create --name $RADICALE_CONTAINER_VOLUME_DATA
    #mkdir -p $RADICALE_CONTAINER_VOLUME_CONFIG

    echo ""
    echo "Check for config..."
    if [ -f "$RADICALE_CONTAINER_VOLUME_CONFIG/config" ]; then
        echo "config file already exists, OK"
    else
        echo "config file does not exists, copy config.template"
	#docker exec -it $RADICALE_CONTAINER_NAME cat /config
	#https://raw.githubusercontent.com/Kozea/Radicale/master/config
	#sudo cp config.template $RADICALE_CONTAINER_VOLUME_CONFIG/config
        #sudo chown -R 0:0 $RADICALE_CONTAINER_VOLUME_CONFIG/config
    fi

    echo ""
    echo "Boot ..."
    docker-compose up -d --remove-orphans $1
else
    echo "File $FILE does not exist: deploying for first time"
    cp .env.template .env
    
    echo "Add HOST_PWD=$PWD to .env"
    echo "" >> .env
    echo "" >> .env
    echo "HOST_PWD=$PWD" >> .env
    
    echo "Add HOST_UID=$UID to .env"
    echo "HOST_UID=$UID" >> .env
    
    echo -e "Make sure to modify .env according to your needs, then use ./up.sh again."
fi
